import 'reflect-metadata';
import {Expose, Type} from 'class-transformer';
import {IsString, IsNotEmpty, ValidateNested, IsNumber} from 'class-validator';

// tslint:disable-next-line:no-namespace
export namespace Request {

    export namespace Add {

        export class User {

            @Expose()
            public id!: number;

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public username!: string;

            @Expose() @IsString() @IsNotEmpty()
            public email!: string;

            @Expose() @IsString()
            public phone!: string;

            @Expose() @IsString()
            public website!: string;

            @Expose() @ValidateNested() @Type(() => Company)
            public company: Company = new Company();
        }

        // tslint:disable-next-line:max-classes-per-file
        class Company {

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public catchPhrase!: string;

            @Expose() @IsString() @IsNotEmpty()
            public bs!: string;
        }
    }

    export namespace Modify {
        // tslint:disable-next-line:max-classes-per-file
        export class User {

            @Expose() @IsNumber() @IsNotEmpty()
            public id!: number;

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public username!: string;

            @Expose() @IsString() @IsNotEmpty()
            public email!: string;

            @Expose() @IsString()
            public phone!: string;

            @Expose() @IsString()
            public website!: string;

            @Expose() @ValidateNested() @Type(() => Company)
            public company: Company = new Company();
        }

        // tslint:disable-next-line:max-classes-per-file
        class Company {

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public catchPhrase!: string;

            @Expose() @IsString() @IsNotEmpty()
            public bs!: string;
        }
    }
}

// tslint:disable-next-line:no-namespace
export namespace Response {

    export namespace FindOne {

        export class User {

            @Expose()
            public id!: number;

            @Expose()
            public name!: string;

            @Expose()
            public username!: string;

            @Expose()
            public email!: string;

            @Expose()
            public phone!: string;

            @Expose()
            public website!: string;

            @Expose() @ValidateNested() @Type(() => Company)
            public company: Company = new Company();
        }

        // tslint:disable-next-line:max-classes-per-file
        class Company {

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public catchPhrase!: string;

            @Expose() @IsString() @IsNotEmpty()
            public bs!: string;
        }
    }


    export namespace FindAll {

        export class User {

            @Expose()
            public id!: number;

            @Expose()
            public name!: string;

            @Expose()
            public username!: string;

            @Expose()
            public email!: string;

            @Expose()
            public phone!: string;

            @Expose()
            public website!: string;

            @Expose() @ValidateNested() @Type(() => Company)
            public company!: Company;
        }

        // tslint:disable-next-line:max-classes-per-file
        class Company {

            @Expose() @IsString() @IsNotEmpty()
            public name!: string;

            @Expose() @IsString() @IsNotEmpty()
            public catchPhrase!: string;

            @Expose() @IsString() @IsNotEmpty()
            public bs!: string;
        }
    }
}
