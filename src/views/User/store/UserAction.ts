import {Actions} from 'vuex-smart-module';
import {UserState} from '@/views/User/store/UserState';
import {UserGetter} from '@/views/User/store/UserGetter';
import {UserMutation} from '@/views/User/store/UserMutation';
import {Request, Response} from '@/views/User/model/UserForm';
import axios from '@/axios';
import router from '@/router';

export class UserAction extends Actions<UserState, UserGetter, UserMutation, UserAction> {

    public async findOne(userId: number) {
        const response = await axios.get(`/users/`, {params: {id: userId}});
        await this.mutations.findOne(Object.$plugins.mapper.toClass(Response.FindOne.User, response.data[0]) as Response.FindOne.User);
    }

    public async findAll() {
        const response = await axios.get(`/users`, {params: {}});
        await this.mutations.findAll(Object.$plugins.mapper.toClass(Response.FindAll.User, response.data) as Array<Response.FindAll.User>);
    }

    public async addUser(addUser: Request.Add.User) {
        const errors = await Object.$plugins.validator.validate(addUser);
        if (Object.$plugins.lodash.isEmpty(errors)) {
            const response = await axios.post(`/users/`, {params: addUser});
            await this.mutations.addUser(response.data);
        } else {
            console.log(errors);
        }
    }

    public async modifyUser(modifyUser: Request.Modify.User) {
        const errors = await Object.$plugins.validator.validate(modifyUser);
        if (Object.$plugins.lodash.isEmpty(errors)) {
            const response = await axios.put(`/users/${modifyUser.id}`, {params: modifyUser});
            await this.mutations.modifyUser(response.data);
        } else {
            console.log(errors);
        }
    }

    public async removeUser(userId: number) {
        const response = await axios.delete(`/users/${userId}`, {params: userId});
        await setTimeout(() => {
            router.push({name: 'userList'});
        }, 2000);
    }

}
