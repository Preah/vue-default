import {Mutations} from 'vuex-smart-module';
import {Request, Response} from '@/views/User/model/UserForm';
import {UserState} from '@/views/User/store/UserState';
import router from '@/router';

export class UserMutation extends Mutations<UserState> {

    public findOne(payload: Response.FindOne.User): void {
        this.state.findOne = payload;
    }

    public findAll(payload: Array<Response.FindAll.User>): void {
        this.state.findAll = payload;
    }

    public addUser(payload: Request.Add.User): void {
        setTimeout(() => {
            router.push({name: 'userList'});
        }, 2000);
    }

    public modifyUser(payload: Request.Modify.User): void {
        setTimeout(() => {
            router.push({name: 'userList'});
        }, 2000);
    }
}
