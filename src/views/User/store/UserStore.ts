import Vue from 'vue';
import Vuex from 'vuex';
import {createStore, Module} from 'vuex-smart-module';
import {UserState} from '@/views/User/store/UserState';
import {UserGetter} from '@/views/User/store/UserGetter';
import {UserMutation} from '@/views/User/store/UserMutation';
import {UserAction} from '@/views/User/store/UserAction';

Vue.use(Vuex);

export const UserContext = new Module({
    state: UserState,
    getters: UserGetter,
    mutations: UserMutation,
    actions: UserAction,
});

export const store = createStore(UserContext);
