import {Response} from '@/views/User/model/UserForm';

export class UserState {

    public findOne: Response.FindOne.User = new Response.FindOne.User;
    public findAll: Array<Response.FindAll.User> = [];
}
