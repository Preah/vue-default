import {Getters} from 'vuex-smart-module';
import {Request, Response} from '@/views/User/model/UserForm';
import {UserState} from '@/views/User/store/UserState';
import {plainToClass} from 'class-transformer';

export class UserGetter extends Getters<UserState> {

    public findOne(): Response.FindOne.User {
        return this.state.findOne;
    }

    public findAll(): Array<Response.FindAll.User> {
        return this.state.findAll;
    }

    public modifyUser(): Request.Modify.User {
        return Object.$plugins.mapper.toClass(Request.Modify.User, this.state.findOne) as Request.Modify.User;
    }
}
