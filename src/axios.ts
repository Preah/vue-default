import axiosLib from 'axios';
import eventBus from '@/eventbus';

const axios = axiosLib.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
    timeout: 10000,
});

// axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
//axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');
// axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

axios.interceptors.request.use(
    (conf) => {
        eventBus.$emit('before-request');
        return conf;
    },
    (error) => {
        eventBus.$emit('request-error');
        return Promise.reject(error);
    },
);
axios.interceptors.response.use(
    (response) => {
        setTimeout(() => {
            eventBus.$emit('after-response');
        }, 1000);
        return response;
    },
    (error) => {
        eventBus.$emit('response-error');
        return Promise.reject(error);
    },
);


export default axios;

