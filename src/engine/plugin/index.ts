import * as validator from 'class-validator';
import * as lodash    from 'lodash';
import {Mapper}       from '@/engine/plugin/Mapper';
import {Plugins}      from '@/engine/plugin/Plugins';

export default {

   install() {
       Object.defineProperty(Object, '$plugins', {
           get : () : Plugins => {
               return new Plugins (
                    new Mapper
                   ,new validator.Validator
                   ,Object.create(lodash)
               );
           }
       });
   },
}
