import {Mapper}       from '@/engine/plugin/Mapper';
import * as validator from 'class-validator';
import * as lodash    from 'lodash';

declare global { 
    interface Object {
        $plugins : IPlugin;
    }

    interface IPlugin {
        mapper    : Mapper;
        validator : validator.Validator;
        lodash    : lodash.LoDashStatic;
    }
}

export class Plugins implements IPlugin {

    constructor(mapper : Mapper, validator : validator.Validator, lodash:lodash.LoDashStatic){
        this.mapper    = mapper;
        this.validator = validator;
        this.lodash    = lodash;
    }

    public lodash!    : lodash.LoDashStatic;
    public mapper!    : Mapper;
    public validator! : validator.Validator;
}
