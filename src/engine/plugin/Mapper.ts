import {plainToClass, ClassTransformer} from 'class-transformer';

export class Mapper extends ClassTransformer {

    public toClass = (type: any, source: object) : object => {
        return plainToClass(type, source, { excludeExtraneousValues: true });
    }
}
