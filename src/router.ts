import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/user/list',
            name: 'userList',
            component: () => import('./views/User/view/List.vue'),
        },
        {
            path: '/user/add',
            name: 'userAdd',
            component: () => import('./views/User/view/Add.vue'),
        },
        {
            path: '/user/:id/modify',
            name: 'userModify',
            component: () => import('./views/User/view/Modify.vue'),
        },
        {
            path: '*',
            component: () => import('./views/User/view/List.vue'),
        },
    ],
});

router.beforeResolve((to, from, next) => {
    if (to.name) {
        // Start the route progress bar.
        // NProgress.start();
    }
    next();
});

router.afterEach((to, from) => {
    // NProgress.done();
});

export default router;
